<?php
/**
 * Created by PhpStorm.
 * User: iclassroom
 * Date: 28/11/2018 AD
 * Time: 09:59
 */
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('users')->delete();
        \Illuminate\Foundation\Auth\User::create(array(
            'name'     => 'sutthiphong',
            'username' => 'job',
            'email'    => 'job@smsu.ac.th',
            'password' => Hash::make('awesome'),
        ));
    }

}
